// create random json file and delete the file
const fs = require('fs')
const path = require('path');

function createDeleteFiles() {
  const dirName = path.join(__dirname, "dir-data");

  const files = Array(10).fill(0)
    .map((item, index) => {
      return `${index}.json`
    });

  // create directory
  fs.mkdir(dirName, (err) => {
    if (err) {
      console.error(err);
    } else {
      console.log("directory created succefully");

      // call create file function
      createFile(dirName, files, (err, fileName) => {
        if (err) {
          console.log("error:", err);
        }
        else {
          console.log(`${fileName} file is created`);
        }
      });
    }

  });

  // create random json file
  const createFile = (dirName, fileName, callback) => {
    for (let index = 0; index < fileName.length; index++) {
      fs.writeFile(path.join(dirName, fileName[index]), JSON.stringify({ a: "123" }), (err) => {
        if (err) {
          console.log("error : ", err);
        } else {

          callback(null, fileName[index]);
          deleteFile(dirName, fileName[index], (err, data) => {
            if (err) {
              console.log("error: ", err);
            } else {
              console.log(`${data} file is deleted`);
            }
          })
        }
      })
    }
  }

  // delete json file
  const deleteFile = (dirName, fileName, callback) => {
    fs.unlink(path.join(dirName, fileName), (err) => {
      if (err) {
        console.log("error: ", err);
      } else {
        callback(null, fileName)
      }
    })
  }

}

module.exports = createDeleteFiles
