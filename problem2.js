const fs = require('fs')
const path = require('path')

function fileOperations() {

    let fileName = "filenames.txt"

    //read file
    function readFile() {

        fs.readFile(path.join(__dirname, "lipsum.txt"), (err, data) => {
            if (err) {
                console.log(err);
            } else {
                let fileData = data.toString();

                //calling fuction to convert upper case
                converUpperCase(fileData, (err, data) => {
                    if (err) {
                        console.log("error: ", err);
                    }
                    else {
                        console.log(data);
                    }
                });
            }
        })
    }

    // to convert all content in upper case
    const converUpperCase = (fileData, callback) => {
        let dataInUpperCase = fileData.toUpperCase()

        fs.writeFile(path.join(__dirname, "dataInUpperCase.txt"), `${dataInUpperCase}`, (err) => {
            if (err) {
                console.error(err);

            } else {
                fs.writeFile(path.join(__dirname, fileName), "dataInUpperCase.txt", (err) => {

                    if (err) {
                        console.error(err);
                    } else {
                        callback(null, "file coverted in upper case")

                        // calling convertLowerCase function 
                        convertLowerCase(dataInUpperCase, (err, data) => {
                            if (err) {
                                console.log("error: ", err);
                            } else {
                                console.log(data);
                            }
                        });
                    }
                })
            }
        })
    }

    // this function convert file data in lower case
    const convertLowerCase = (upperCaseData, callback) => {

        let dataInLowerCase = upperCaseData.toLowerCase();
        let splitData = dataInLowerCase.split('.');

        fs.writeFile(path.join(__dirname, "loweCaseAndSplitData.txt"), JSON.stringify(`${splitData}`), (err) => {
            if (err) {
                console.error(err);
            } else {

                fs.appendFile(path.join(__dirname, fileName), " loweCaseAndSplitData.txt", (err) => {
                    if (err) {
                        console.error(err);
                    } else {
                        callback(null, "data coverted in lower case")

                        //calling sortedContent
                        sortedContent("loweCaseAndSplitData.txt", (err, data) => {
                            if (err) {
                                console.log("error:", err);
                            } else {
                                console.log(data);
                            }
                        });
                    }
                })
            }
        })
    }

    // to sorted the content of dataInUpperCase.txt and splitData.txt
    const sortedContent = (lowerCase, callback) => {
        let filesData = [];
        fs.readFile(path.join(__dirname, "dataInUpperCase.txt"), (err, data) => {
            if (err) {
                console.error(err);
            } else {
                filesData = (data.toString()).split(". ");

                filesData = filesData.sort((first, second) => {
                    return first.localeCompare(second)
                })

                fs.writeFile(path.join(__dirname, "sortedData.txt"), JSON.stringify(filesData), (err) => {
                    if (err) {
                        console.error(err);
                    } else {
                        fs.readFile(path.join(__dirname, lowerCase), (err, data) => {
                            if (err) {
                                console.error(err);
                            } else {
                                filesData = data.toString().split(" ");

                                filesData = filesData.sort((first, second) => {
                                    return first.localeCompare(second)
                                })

                                fs.appendFile(path.join(__dirname, "sortedData.txt"), JSON.stringify(filesData), (err) => {
                                    if (err) {
                                        console.error(err);
                                    } else {
                                        fs.appendFile(path.join(__dirname, fileName), " sortedData.txt", (err) => {
                                            if (err) {
                                                console.error(err);
                                            } else {
                                                callback(null, "data sorted")

                                                deleteAllFile();  //calling deteAllFile() function
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                })


            }
        })
    }

    // this function delete all file which file name store in the filenames.txt
    function deleteAllFile() {
        fs.readFile(path.join(__dirname, fileName), (err, data) => {
            if (err) {
                console.error(err);
            } else {
                fileNames = (data.toString()).split(' ')
                
                for (let index = 0; index < fileNames.length; index++) {

                    fs.unlink(path.join(__dirname, fileNames[index]), (err) => {
                        if (err) {
                            console.log("error:", err);
                        }else{
                            console.log(fileNames[index]," file is deleted");
                        }
                    })

                }

            }
        })
    }


    readFile();

}

module.exports = fileOperations;